package com.example.tictactoespringtdd.util;

import com.example.tictactoespringtdd.dto.Game;
import com.example.tictactoespringtdd.model.GameEntity;
import com.example.tictactoespringtdd.model.MarkerEnum;

public class GameEntityTransformer {

    public Game apply(GameEntity gameEntity) {
        String nextPlayer = gameEntity.getNextPlayer() == MarkerEnum.BLANK ? "X can start the game" : String.format("%s Player", gameEntity.getNextPlayer());
        return Game.builder()
                .id(gameEntity.getId())
                .endGame(gameEntity.isEndGame())
                .nextPlayer(nextPlayer)
                .build();
    }
}
