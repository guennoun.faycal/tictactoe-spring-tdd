package com.example.tictactoespringtdd.controller;

import com.example.tictactoespringtdd.dto.Game;
import com.example.tictactoespringtdd.dto.PlayGame;
import com.example.tictactoespringtdd.service.GameService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v0/tictactoe")
@AllArgsConstructor
@Slf4j
public class GameController {

    private final GameService gameService;

    @GetMapping("/new")
    public ResponseEntity<Game> createNewGame() {
        log.info("Create new Tic Tac Toe Game.");
        Game game = gameService.createGame();
        log.info("Game Id: {}\n", game.getId());

        return ResponseEntity.ok(game);
    }

    @PostMapping("/play")
    public ResponseEntity playGame(@RequestBody PlayGame playGame) {

        log.info("Play game {}", playGame);
        if (playGame.getCol() < 0 || playGame.getCol() > 2 || playGame.getRow() < 0 || playGame.getRow() > 2) {
            throw new IllegalArgumentException("Wrong row or column information, they should be between 0 and 2!");
        }
        if (!"X".equals(playGame.getPlayer()) && !"O".equals(playGame.getPlayer())) {
            throw new IllegalArgumentException("Wrong player name, it should be X or O.");
        }
        return ResponseEntity.ok(gameService.play(playGame));

    }
}
