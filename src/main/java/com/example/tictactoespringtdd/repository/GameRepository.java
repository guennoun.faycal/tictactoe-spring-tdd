package com.example.tictactoespringtdd.repository;

import com.example.tictactoespringtdd.model.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GameRepository extends JpaRepository<GameEntity, UUID> {
}