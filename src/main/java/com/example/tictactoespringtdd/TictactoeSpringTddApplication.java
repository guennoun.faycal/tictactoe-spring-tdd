package com.example.tictactoespringtdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TictactoeSpringTddApplication {

    public static void main(String[] args) {
        SpringApplication.run(TictactoeSpringTddApplication.class, args);
    }

}
